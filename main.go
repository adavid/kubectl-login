package main

import (
	"fmt"
	"os"

	"gitlab.com/adavid/kubectl-login/cli"
)

const version = "0.1.1"

func main() {

	cli, err := cli.Parse(os.Args, version)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(2)
	}
	if err := cli.Run(); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(2)
	}
}
