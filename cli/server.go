package cli

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
	"time"

	oidc "github.com/coreos/go-oidc"
	"gitlab.com/adavid/kubectl-login/kubeconfig"
	"golang.org/x/oauth2"
)

// Config represent OIDC configuration.
type Config struct {
	Issuer       string
	Listen       *url.URL
	RootCAs      string
	Debug        bool
	ClientID     string
	ClientSecret string
	RedirectURI  string

	Ctx      context.Context
	Verifier *oidc.IDTokenVerifier
	Provider *oidc.Provider

	// Does the provider use "offline_access" scope to request a refresh token
	// or does it use "access_type=offline" (e.g. Google)?
	OfflineAsScope bool

	Client *http.Client
	Server *http.Server
}

var srvConfig = &Config{}

// return an HTTP client which trusts the provided root CAs.
func (c *Config) httpClientForRootCAs(rootCAs string) error {
	tlsConfig := tls.Config{RootCAs: x509.NewCertPool()}
	rootCABytes, err := ioutil.ReadFile(rootCAs)
	if err != nil {
		return fmt.Errorf("failed to read root-ca: %v", err)
	}
	if !tlsConfig.RootCAs.AppendCertsFromPEM(rootCABytes) {
		return fmt.Errorf("no certs found in root CA file %q", rootCAs)
	}
	c.Client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tlsConfig,
			Proxy:           http.ProxyFromEnvironment,
			Dial: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
			}).Dial,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}
	return nil
}

type debugTransport struct {
	t http.RoundTripper
}

// RoundTrip Debug
func (d debugTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	reqDump, err := httputil.DumpRequest(req, true)
	if err != nil {
		return nil, err
	}
	log.Printf("%s", reqDump)

	resp, err := d.t.RoundTrip(req)
	if err != nil {
		return nil, err
	}

	respDump, err := httputil.DumpResponse(resp, true)
	if err != nil {
		resp.Body.Close()
		return nil, err
	}
	log.Printf("%s", respDump)
	return resp, nil
}

// SetProvider set configuration for the provider
func (c *Config) SetProvider(issuerURL string) error {
	ctx := oidc.ClientContext(context.Background(), c.Client)
	provider, err := oidc.NewProvider(ctx, issuerURL)
	if err != nil {
		return err
	}
	var s struct {
		ScopesSupported []string `json:"scopes_supported"`
	}
	if err := provider.Claims(&s); err != nil {
		return fmt.Errorf("Failed to parse provider scopes_supported: %v", err)
	}
	if len(s.ScopesSupported) == 0 {
		// scopes_supported is a "RECOMMENDED" discovery claim, not a required
		// one. If missing, assume that the provider follows the spec and has
		// an "offline_access" scope.
		c.OfflineAsScope = true
	} else {
		// See if scopes_supported has the "offline_access" scope.
		c.OfflineAsScope = func() bool {
			for _, scope := range s.ScopesSupported {
				if scope == oidc.ScopeOfflineAccess {
					return true
				}
			}
			return false
		}()
	}
	c.Provider = provider
	c.Verifier = provider.Verifier(&oidc.Config{ClientID: c.ClientID})
	return nil
}

//HTTPServer Launch server
func (c *Config) HTTPServer() error {
	log.Printf("listening on %s", c.Listen.String())
	c.Server = &http.Server{Addr: c.Listen.Host}
	if err := c.Server.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("ListenAndServe: %s", err)
	}
	return nil
}

//Shutdown http shutdown
func (c *Config) Shutdown() error {
	c.Server.Shutdown(c.Ctx)
	return nil
}

func (c *Config) oauth2Config(scopes []string) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     c.ClientID,
		ClientSecret: c.ClientSecret,
		Endpoint:     c.Provider.Endpoint(),
		Scopes:       scopes,
		RedirectURL:  c.RedirectURI,
	}
}

func (c *Config) handleLogin(w http.ResponseWriter, r *http.Request) {

	scopes := []string{"groups", "openid", "profile", "email", "offline_access"}
	authCodeURL := c.oauth2Config(scopes).AuthCodeURL(appState)

	v := url.Values{}

	req, _ := http.NewRequest("GET", authCodeURL, strings.NewReader(v.Encode()))
	//http.Redirect(w, r, authCodeURL, http.StatusSeeOther)

	resp, err := c.Client.Do(req)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error : %v\n", err)
		return
	}
	http.Redirect(w, r, resp.Request.URL.String(), http.StatusSeeOther)

}

func (c *Config) handleCallback(w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		token *oauth2.Token
	)

	ctx := oidc.ClientContext(r.Context(), c.Client)
	oauth2Config := c.oauth2Config(nil)
	switch r.Method {
	case http.MethodGet:
		// Authorization redirect callback from OAuth2 auth flow.
		if errMsg := r.FormValue("error"); errMsg != "" {
			http.Error(w, errMsg+": "+r.FormValue("error_description"), http.StatusBadRequest)
			return
		}
		code := r.FormValue("code")
		if code == "" {
			http.Error(w, fmt.Sprintf("no code in request: %q", r.Form), http.StatusBadRequest)
			return
		}
		if state := r.FormValue("state"); state != appState {
			http.Error(w, fmt.Sprintf("expected state %q got %q", appState, state), http.StatusBadRequest)
			return
		}
		token, err = oauth2Config.Exchange(ctx, code)
	case http.MethodPost:
		// Form request from frontend to refresh a token.
		refresh := r.FormValue("refresh_token")
		if refresh == "" {
			http.Error(w, fmt.Sprintf("no refresh_token in request: %q", r.Form), http.StatusBadRequest)
			return
		}
		t := &oauth2.Token{
			RefreshToken: refresh,
			Expiry:       time.Now().Add(-time.Hour),
		}
		token, err = oauth2Config.TokenSource(ctx, t).Token()
	default:
		http.Error(w, fmt.Sprintf("method not implemented: %s", r.Method), http.StatusBadRequest)
		return
	}

	if err != nil {
		http.Error(w, fmt.Sprintf("failed to get token: %v", err), http.StatusInternalServerError)
		return
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "no id_token in token response", http.StatusInternalServerError)
		return
	}

	idToken, err := c.Verifier.Verify(r.Context(), rawIDToken)
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to verify ID token: %v", err), http.StatusInternalServerError)
		return
	}
	var claims json.RawMessage
	idToken.Claims(&claims)

	buff := new(bytes.Buffer)
	json.Indent(buff, []byte(claims), "", "  ")

	authProvider.SetIDToken(rawIDToken)
	authProvider.SetRefreshToken(token.RefreshToken)
	err = kubeconfig.Write(cfg, cli.KubeConfig)
	if err != nil {
		log.Printf("Configuration writing error : %v", err)
		return
	}
	log.Printf("Configuration updated")
	w.Header().Add("Content-Type", "text/html")
	fmt.Fprintf(w, `<!DOCTYPE html><html><head><script>window.close()</script></head><body>OK</body></html>`)
	if f, ok := w.(http.Flusher); ok {
		f.Flush()
	}
	time.Sleep(2 * time.Second)
	c.Shutdown()
}
