package cli

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"

	flags "github.com/jessevdk/go-flags"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/pkg/browser"
	"gitlab.com/adavid/kubectl-login/kubeconfig"
	"k8s.io/client-go/tools/clientcmd/api"
)

const appState = "txAfMtl5EKQEElzCaxWb"

// CLI represents an interface of this command.
// "listen", "http://localhost:8000", "HTTP(S) address to listen at."
type CLI struct {
	KubeConfig      string `long:"kubeconfig" default:"~/.kube/config" env:"KUBECONFIG" description:"Path to the kubeconfig file"`
	User            string `long:"user" default:"" env:"KUBECTL_LOGIN_USER" description:"User to use for"`
	Listen          string `long:"listen" default:"http://localhost:8000" env:"KUBECTL_LOGIN_LISTEN" description:"HTTP(S) address to listen at"`
	SkipTLSVerify   bool   `long:"insecure-skip-tls-verify" env:"KUBECTL_LOGIN_INSECURE_SKIP_TLS_VERIFY" description:"If set, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure"`
	SkipOpenBrowser bool   `long:"skip-open-browser" env:"KUBECTL_LOGIN_SKIP_OPEN_BROWSER" description:"If set, it does not open the browser on authentication."`
	RootCAs         string `long:"issuer-root-ca" env:"KUBECTL_ROOT_CA" description:"Root certificate authorities for the issuer. Defaults to host certs."`
}

var (
	authProvider *kubeconfig.OIDCAuthProvider
	cfg          *api.Config
	cli          CLI
)

// Parse parses command line arguments and returns a CLI instance.
func Parse(osArgs []string, version string) (*CLI, error) {
	parser := flags.NewParser(&cli, flags.HelpFlag)
	parser.LongDescription = fmt.Sprintf(`Version %s
		This updates the kubeconfig for Kubernetes OpenID Connect (OIDC) authentication.`,
		version)
	args, err := parser.ParseArgs(osArgs[1:])
	if err != nil {
		return nil, err
	}
	if len(args) > 0 {
		return nil, fmt.Errorf("Too many argument")
	}
	return &cli, nil
}

// ExpandKubeConfig reset path or return error
func (c *CLI) ExpandKubeConfig() error {
	d, err := homedir.Expand(c.KubeConfig)
	if err != nil {
		return fmt.Errorf("Could not expand %s: %s", c.KubeConfig, err)
	}
	c.KubeConfig = d
	return nil
}

// Run performs this command.
func (c *CLI) Run() error {
	log.Printf("Reading %s", c.KubeConfig)
	err := c.ExpandKubeConfig()
	if err != nil {
		return err
	}
	cfg, err = kubeconfig.Read(c.KubeConfig)
	if err != nil {
		return err
	}
	authProvider, err = kubeconfig.FindOIDCAuthProvider(cfg, c.User)
	if err != nil {
		return fmt.Errorf(`Could not find OIDC configuration in kubeconfig: %s
			Did you setup kubectl for OIDC authentication?
				kubectl config set-credentials %s \
					--auth-provider oidc \
					--auth-provider-arg idp-issuer-url=https://issuer.example.com \
					--auth-provider-arg client-id=YOUR_CLIENT_ID \
					--auth-provider-arg client-secret=YOUR_CLIENT_SECRET`,
			err, cfg.CurrentContext)
	}
	listenURL, err := url.Parse(c.Listen)
	if err != nil {
		return fmt.Errorf("parse listen address: %v", err)
	}

	tlsConfig := c.tlsConfig()
	srvConfig = &Config{
		Issuer:       authProvider.IDPIssuerURL(),
		Listen:       listenURL,
		ClientID:     authProvider.ClientID(),
		ClientSecret: authProvider.ClientSecret(),
		Client:       &http.Client{Transport: &http.Transport{TLSClientConfig: tlsConfig}},
		RedirectURI:  listenURL.String() + "/callback",
		Ctx:          context.Background(),
	}
	http.HandleFunc("/", srvConfig.handleLogin)
	http.HandleFunc("/callback", srvConfig.handleCallback)
	err = srvConfig.httpClientForRootCAs(authProvider.IDPCertificateAuthority())
	if err != nil {
		return fmt.Errorf("httpClientForRootCAs : %v", err)
	}
	err = srvConfig.SetProvider(authProvider.IDPIssuerURL())
	if err != nil {
		return fmt.Errorf("Set provider : %v", err)
	}
	errCh := make(chan error)
	defer close(errCh)
	go func() {
		errCh <- srvConfig.HTTPServer()
	}()
	go func() {
		time.Sleep(500 * time.Millisecond)
		if !cli.SkipOpenBrowser {
			browser.OpenURL(c.Listen)
		}
	}()
	select {
	case err := <-errCh:
		return err
	case <-srvConfig.Ctx.Done():
		return nil
	}

	//return nil
}
